import React, { Component } from "react";
import {
  Grid,
  TableHeaderRow,
  Table,
  Toolbar,
  SearchPanel
} from "@devexpress/dx-react-grid-material-ui";
import { SearchState, IntegratedFiltering } from "@devexpress/dx-react-grid";
import { observer, inject } from "mobx-react";
import { currencyFormatter } from "../../helper/functions";
import { toJS } from "mobx";

class VehicleList extends Component {
  componentDidMount() {
    const { vehicleStore } = this.props;
    const { vehicleArrayList } = vehicleStore;

    localStorage.setItem("vehicleArray", JSON.stringify(vehicleArrayList));
  }

  handleRowClick = row => {
    const { history } = this.props;
    const vehicleId = row.id;
    history.push(`/vehicle/${vehicleId}`);
  };

  Cell = props => {
    if (props.column.name === "price") {
      return (
        <Table.Cell {...props}>
          <span>{currencyFormatter(props.value)}</span>
        </Table.Cell>
      );
    }
    if (props.column.name === "type") {
      if (props.value === "sedan") {
        return (
          <Table.Cell {...props}>
            <span>Sedan</span>
          </Table.Cell>
        );
      } else {
        return (
          <Table.Cell {...props}>
            <span>SUV</span>
          </Table.Cell>
        );
      }
    }
    return <Table.Cell {...props} />;
  };

  TableRow = ({ row, ...restProps }) => (
    <Table.Row
      {...restProps}
      onClick={e => {
        e.preventDefault();
        this.handleRowClick(row);
      }}
      style={{
        cursor: "pointer"
      }}
    />
  );

  renderVehicleListGrid() {
    const columns = [
      { name: "id", title: "ID", key: true },
      { name: "name", title: "Nama" },
      { name: "type", title: "Type" },
      { name: "price", title: "Price" }
    ];
    const { vehicleStore } = this.props;
    const { vehicleArrayList } = vehicleStore;

    return (
      <Grid rows={vehicleArrayList} columns={columns}>
        <h3 className="title">Vehicle List</h3>
        <SearchState />
        <IntegratedFiltering />
        <Table cellComponent={this.Cell} rowComponent={this.TableRow} />
        <TableHeaderRow />
        <Toolbar />
        <SearchPanel />
      </Grid>
    );
  }

  render() {
    return (
      <div className="animated fadeIn main-container">
        {this.renderVehicleListGrid()}
      </div>
    );
  }
}

export default inject("vehicleStore")(observer(VehicleList));
