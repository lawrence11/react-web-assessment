import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";

class VehicleDetails extends Component {
  handleChange = (event, index) => {
    const { name, value } = event.target;
    const { vehicleStore } = this.props;

    vehicleStore.setVehicleData(name, value, index);
  };

  handleSave = () => {
    const { history, vehicleStore } = this.props;
    const { vehicleArrayList } = vehicleStore;

    JSON.stringify(localStorage.setItem("vehicleArray", vehicleArrayList));
    history.push("/vehicle");
  };

  render() {
    const { vehicleStore, match } = this.props;
    const { params } = match;
    const { vehicleId } = params;
    const { vehicleArrayList, vehicleTypeEnum } = vehicleStore;

    return (
      <div className="animated fadeIn main-container">
        <div className="edit-container">
          <h3 className="title">Edit Vehicle Detail</h3>
          {vehicleArrayList.map((data, index) => {
            if (data.id == vehicleId) {
              return (
                <React.Fragment>
                  <TextField
                    name="name"
                    label="Name"
                    placeholder="Input car name"
                    value={data.name ? data.name : ""}
                    onChange={event => {
                      this.handleChange(event, index);
                    }}
                    margin="normal"
                    variant="outlined"
                  />
                  <TextField
                    name="type"
                    label="Car Type"
                    placeholder="Choose car type"
                    value={data.type ? data.type : ""}
                    onChange={event => {
                      this.handleChange(event, index);
                    }}
                    select
                    margin="normal"
                    variant="outlined"
                  >
                    {vehicleTypeEnum.map(option => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.name}
                      </MenuItem>
                    ))}
                  </TextField>
                  <TextField
                    name="price"
                    label="Price"
                    placeholder="Input car price"
                    value={data.price ? data.price : ""}
                    onChange={event => {
                      this.handleChange(event, index);
                    }}
                    margin="normal"
                    variant="outlined"
                  />
                </React.Fragment>
              );
            }
          })}
          <button className="save-btn" onClick={this.handleSave}>
            Save
          </button>
        </div>
      </div>
    );
  }
}

export default inject("vehicleStore")(observer(VehicleDetails));
