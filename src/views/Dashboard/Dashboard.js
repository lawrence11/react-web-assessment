import React, { Component } from "react";
import {
  Grid,
  TableHeaderRow,
  Table,
  Toolbar,
  SearchPanel
} from "@devexpress/dx-react-grid-material-ui";
import { SearchState, IntegratedFiltering } from "@devexpress/dx-react-grid";
import { observer, inject } from "mobx-react";
import { Tab, Tabs } from "@material-ui/core";
import { toJS } from "mobx";

class Dashboard extends Component {
  state = {
    selectedIndex: 0
  };

  handleChange = (event, value) => {
    this.setState({ selectedIndex: value });
  };

  handleRejectOrder = async id => {
    const { orderStore } = this.props;
    await orderStore.setPendingOrderStatus(id, "rejected");
    localStorage.setItem(
      "pendingArray",
      JSON.stringify(orderStore.pendingOrderArray)
    );
  };

  handleConfirmOrder = async id => {
    const { orderStore } = this.props;
    await orderStore.setPendingOrderStatus(id, "confirmed");
    localStorage.setItem(
      "pendingArray",
      JSON.stringify(orderStore.pendingOrderArray)
    );
    localStorage.setItem(
      "confirmedArray",
      JSON.stringify(orderStore.confirmedOrderArray)
    );
  };

  Cell = props => {
    if (props.column.name == "action") {
      if (props.row.status == "pending") {
        return (
          <Table.Cell {...props}>
            <div>
              <button
                onClick={() => {
                  this.handleRejectOrder(props.row.id);
                }}
                className="reject-btn"
              >
                Reject
              </button>
              <button
                onClick={() => {
                  this.handleConfirmOrder(props.row.id);
                }}
                className="confirm-btn"
              >
                Confirm
              </button>
            </div>
          </Table.Cell>
        );
      }
    }
    return <Table.Cell {...props} />;
  };

  renderPendingGrid() {
    const columns = [
      { name: "id", title: "ID", key: true },
      { name: "name", title: "Nama" },
      { name: "status", title: "Status" },
      { name: "action", title: "Action" }
    ];
    const { orderStore } = this.props;
    const { pendingOrderArray } = orderStore;

    return (
      <Grid rows={toJS(pendingOrderArray)} columns={columns}>
        <h3 className="title">Pending Order List</h3>
        <SearchState />
        <IntegratedFiltering />
        <Table cellComponent={this.Cell} />
        <TableHeaderRow />
        <Toolbar />
        <SearchPanel />
      </Grid>
    );
  }

  renderConfirmedGrid() {
    const columns = [
      { name: "id", title: "ID", key: true },
      { name: "name", title: "Nama" },
      { name: "status", title: "Status" }
    ];
    const { orderStore } = this.props;
    const { confirmedOrderArray } = orderStore;

    return (
      <Grid rows={confirmedOrderArray} columns={columns}>
        <h3 className="title">Confirmed Order List</h3>
        <SearchState />
        <IntegratedFiltering />
        <Table cellComponent={this.Cell} />
        <TableHeaderRow />
        <Toolbar />
        <SearchPanel />
      </Grid>
    );
  }

  renderPastGrid() {
    const columns = [
      { name: "id", title: "ID", key: true },
      { name: "name", title: "Nama" },
      { name: "status", title: "Status" }
    ];
    const { orderStore } = this.props;
    const { pastOrderArray } = orderStore;

    return (
      <Grid rows={pastOrderArray} columns={columns}>
        <h3 className="title">Past Order List</h3>
        <SearchState />
        <IntegratedFiltering />
        <Table cellComponent={this.Cell} />
        <TableHeaderRow />
        <Toolbar />
        <SearchPanel />
      </Grid>
    );
  }

  render() {
    const { selectedIndex } = this.state;
    return (
      <div className="animated fadeIn main-container">
        <Tabs
          value={selectedIndex}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab disableRipple label="Pending Orders" />
          <Tab disableRipple label="Confirmed Orders" />
          <Tab disableRipple label="Past Orders" />
        </Tabs>
        {selectedIndex == 0 && this.renderPendingGrid()}
        {selectedIndex == 1 && this.renderConfirmedGrid()}
        {selectedIndex == 2 && this.renderPastGrid()}
      </div>
    );
  }
}

export default inject("orderStore")(observer(Dashboard));
