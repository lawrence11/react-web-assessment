import Dashboard from "./Dashboard";
import MyProfile from "./Profile/MyProfile";
import VehicleList from "./Vehicle/VehicleList";
import VehicleDetails from "./Vehicle/VehicleDetails";

export { Dashboard, MyProfile, VehicleList, VehicleDetails };
