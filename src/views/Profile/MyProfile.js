import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import TextField from "@material-ui/core/TextField";
import { toJS } from "mobx";

class MyProfile extends Component {
  handleChange = event => {
    const { profileStore } = this.props;
    const { name, value } = event.target;

    profileStore.setProfileData({ [name]: value });
  };

  handleSave = () => {
    const { history, profileStore } = this.props;
    const { profileData } = profileStore;
    localStorage.setItem("userData", JSON.stringify(profileData));
    history.push("/");
  };

  render() {
    const { profileStore } = this.props;
    const { profileData } = profileStore;

    return (
      <div className="animated fadeIn main-container">
        <div className="profile-container">
          <h3 className="title">Edit Your Profile</h3>
          <TextField
            name="name"
            label="Name"
            placeholder="Input your name"
            value={profileData.name ? profileData.name : ""}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            name="email"
            label="Email"
            placeholder="Input your emal"
            value={profileData.email ? profileData.email : ""}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <TextField
            name="phoneNumber"
            label="Phone Number"
            placeholder="Input your phone number"
            value={profileData.phoneNumber ? profileData.phoneNumber : ""}
            onChange={this.handleChange}
            margin="normal"
            variant="outlined"
          />
          <button onClick={this.handleSave} className="save-btn">
            Save
          </button>
        </div>
      </div>
    );
  }
}

export default inject("profileStore")(observer(MyProfile));
