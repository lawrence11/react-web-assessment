import { action, decorate, observable, computed, toJS } from "mobx";

class ProfileStore {
  profileData = localStorage.getItem("userData")
    ? JSON.parse(localStorage.getItem("userData"))
    : {
        name: "test user",
        email: "test@test.com",
        phoneNumber: "08123456789"
      };

  setProfileData = newData => {
    this.profileData = {
      ...this.profileData,
      ...newData
    };
  };
}

decorate(ProfileStore, {
  profileData: observable,

  setProfileData: action
});

export default new ProfileStore();
