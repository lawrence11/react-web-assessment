import orderStore from "./OrderStore";
import profileStore from "./ProfileStore";
import vehicleStore from "./VehicleStore";

export default {
  orderStore,
  profileStore,
  vehicleStore
};
