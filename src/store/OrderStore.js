import { action, decorate, observable, computed, toJS } from "mobx";

class OrderStore {
  pendingOrderArray = localStorage.getItem("pendingArray")
    ? JSON.parse(localStorage.getItem("pendingArray"))
    : [
        {
          id: 1,
          name: "Pending 1",
          status: "pending",
          action: ""
        },
        {
          id: 4,
          name: "Pending 2",
          status: "pending",
          action: ""
        }
      ];

  confirmedOrderArray = localStorage.getItem("confirmedArray")
    ? JSON.parse(localStorage.getItem("confirmedArray"))
    : [
        {
          id: 2,
          name: "Confirmed 1",
          status: "confirmed"
        },
        {
          id: 6,
          name: "Confirmed 2",
          status: "confirmed"
        }
      ];

  pastOrderArray = [
    {
      id: 3,
      name: "Past 1",
      status: "completed"
    },
    {
      id: 5,
      name: "Past 2",
      status: "completed"
    }
  ];

  selectedIndex = 0;

  setPendingOrderStatus = (id, status) => {
    this.pendingOrderArray.map((data, index) => {
      if (data.id === id) {
        data.status = status;
        if (status === "confirmed") {
          this.confirmedOrderArray.push(data);
          this.pendingOrderArray.splice(index, 1);
        }
      }
    });
  };
}

decorate(OrderStore, {
  pendingOrderArray: observable,
  confirmedOrderArray: observable,
  pastOrderArray: observable,
  selectedIndex: observable,

  setPendingOrderStatus: action
});

export default new OrderStore();
