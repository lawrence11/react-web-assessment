import { action, decorate, observable, computed, toJS } from "mobx";

class VehicleStore {
  vehicleTypeEnum = [
    {
      name: "Sedan",
      value: "sedan"
    },
    {
      name: "SUV",
      value: "suv"
    }
  ];
  vehicleArrayList = localStorage.getItem("vehicleArray")
    ? JSON.parse(localStorage.getItem("vehicleArray"))
    : [
        {
          id: 1,
          name: "Car 1",
          type: "sedan",
          price: "200000"
        },
        {
          id: 2,
          name: "Car 2",
          type: "suv",
          price: "500000"
        }
      ];

  setVehicleData = (name, value, index) => {
    this.vehicleArrayList[index][name] = value;
  };
}

decorate(VehicleStore, {
  vehicleArrayList: observable,

  setSelectedData: action
});

export default new VehicleStore();
