const numeral = require("numeral");

export function currencyFormatter(amount) {
  if (!amount) {
    return "";
  }
  return "Rp " + numeral(amount).format("0,0");
}
