import React, { Component } from "react";
import { Router, Route, Switch, withRouter } from "react-router-dom";
import { createBrowserHistory } from "history";
// import { renderRoutes } from 'react-router-config';
import Loadable from "react-loadable";
import { Provider } from "mobx-react";
import stores from "./store";
import "./App.scss";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

// Containers
const DefaultLayout = Loadable({
  loader: () => import("./containers/DefaultLayout"),
  loading
});

class App extends Component {
  render() {
    const Root = () => (
      <Switch>
        <Route path="/" name="Home" component={DefaultLayout} />
      </Switch>
    );

    const browserHistory = createBrowserHistory();
    const RouterAssessment = withRouter(Root);
    return (
      <Provider {...stores}>
        <Router history={browserHistory}>
          <RouterAssessment />
        </Router>
      </Provider>
    );
  }
}

export default App;
