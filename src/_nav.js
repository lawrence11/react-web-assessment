export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-home"
    },
    {
      name: "Vehicle Inventory",
      url: "/vehicle",
      icon: "icon-briefcase"
    }
  ]
};
