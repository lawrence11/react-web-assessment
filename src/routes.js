import React from "react";
import DefaultLayout from "./containers/DefaultLayout";

const Dashboard = React.lazy(() => import("./views/Dashboard"));
const MyProfile = React.lazy(() => import("./views/Profile/MyProfile"));
const VehicleList = React.lazy(() => import("./views/Vehicle/VehicleList"));
const VehicleDetails = React.lazy(() =>
  import("./views/Vehicle/VehicleDetails")
);

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Home", component: DefaultLayout },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/my-profile", name: "My Profile", component: MyProfile },
  {
    path: "/vehicle",
    exact: true,
    name: "Vehicle List",
    component: VehicleList
  },
  {
    path: "/vehicle/:vehicleId",
    name: "Vehicle Details",
    component: VehicleDetails
  }
];

export default routes;
